# dev-cas, fake implementation of CAS server

## Goals

- Use a fake CAS server to develop application at localhost keeping a CAS authentication.
- Implement only the needed URI.
- Use specifications from [Apereo](https://apereo.github.io/cas/6.5.x/protocol/CAS-Protocol-Specification.html).
- Install it on Tomcat.

## 🛠️ Tech Stack

- Jakarta 10
- Java 17
- [Maven](https://maven.apache.org/) 3.8
- [Log4j2](https://logging.apache.org/log4j/2.x/index.html)
- [Lombok](https://projectlombok.org/)

## Development

1. Package with `mvn package`.
2. Run the embedded Tomcat with `mvn cargo:run -Plocal`.
3. Change the files.
4. Package again `mvn package`, Tomcat reloads.

Ensure code is well written with `mvn checkstyle:checkstyle pmd:pmd pmd:cpd spotbugs:spotbugs`.

## 🛠️ Installation

Use Maven to deploy on Tomcat. Eg. with server `siclima-preprod`:

```mvn cargo:deploy -Premote -Dcargo.server.settings=siclima-preprod```

with `~/.m2/settings.xml` with something like:

```xml
<servers>
  <server>
    <id>siclima-preprod</id>
    <configuration>
      <cargo.remote.uri>http://siclima-preprod:8081/manager/text</cargo.remote.uri>
      <cargo.remote.username>tomcat-user</cargo.remote.username>
      <cargo.remote.password>tomcat-password</cargo.remote.password>
    </configuration>
  </server>
<servers>
```

## Usage

Simply use these URL to configure CAS in the app (Example with server `siclima-preprod`):

- <http://siclima-preprod:8081/dev-cas/> : root URL for the CAS server.
- <http://siclima-preprod:8081/dev-cas/login> : to build the login process URL
- <http://siclima-preprod:8081/dev-cas/logout> : to logout the user 

Project reports are available using Maven: `mvn site` and then see `target/site/index.html`.

## Authors

See [`AUTHORS`](AUTHORS) file.

## License

See [`LICENSE`](LICENSE) file.
