<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>fr.inrae.agroclim</groupId>
	<artifactId>dev-cas</artifactId>
	<version>1.0.0-SNAPSHOT</version>
	<packaging>war</packaging>
	<name>Fake CAS</name>
	<description>Fake CAS server for development purpose</description>
	<inceptionYear>2022</inceptionYear>
	<organization>
		<url>https://agroclim.inrae.fr/</url>
		<name>Agroclim</name>
	</organization>
	<licenses>
		<license>
			<name>GNU General Public License</name>
			<url>https://www.gnu.org/licenses/gpl-3.0.txt</url>
		</license>
	</licenses>
	<scm>
		<url>https://forgemia.inra.fr/agroclim/dev-cas.git</url>
		<connection>scm:git:https://forgemia.inra.fr/agroclim/dev-cas.git</connection>
		<tag>main</tag>
	</scm>
	<issueManagement>
		<system>Gitlab</system>
		<url>https://forgemia.inra.fr/agroclim/dev-cas/-/issues</url>
	</issueManagement>
	<ciManagement>
		<system>Jenkins</system>
		<url>http://jenkins:8080/job/dev-cas/</url>
	</ciManagement>
	<properties>
		<!-- Dependencies versions -->
		<jstl.version>3.0.0</jstl.version>
		<junit.version>5.10.3</junit.version>
		<log4j.version>2.24.3</log4j.version>
		<lombok.version>1.18.36</lombok.version>
		<servlet-api.version>6.1.0</servlet-api.version>

		<!-- Maven environment values -->
		<build.date>${maven.build.timestamp}</build.date>
		<maven.build.timestamp.format>yyyy-MM-dd HH:mm:ss</maven.build.timestamp.format>
		<maven.compiler.release>17</maven.compiler.release>
		<java.version>17</java.version>
		<maven.compiler.source>17</maven.compiler.source>
		<maven.compiler.target>17</maven.compiler.target>

		<!-- checkStyle -->
		<checkstyle.config.location>file://${basedir}/config/sun_checks.xml</checkstyle.config.location>
		<checkstyle.failsOnError>false</checkstyle.failsOnError>
		<checkstyle.includeResources>false</checkstyle.includeResources>
		<checkstyle.includeTestResources>false</checkstyle.includeTestResources>
		<checkstyle.includeTestSourceDirectory>true</checkstyle.includeTestSourceDirectory>
		<!-- Fix checkstyle version to ensure sun_checks.xml matches. -->
		<checkstyle.version>3.3.1</checkstyle.version>
		<javadoc.version>3.6.2</javadoc.version>
		<javase.api.link>http://docs.oracle.com/javase/8/docs/api</javase.api.link>
		<pmd.version>3.21.2</pmd.version>
		<spotbugs.version>4.8.2.0</spotbugs.version>

		<!-- Text format -->
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
	</properties>
	<dependencies>
		<dependency>
			<groupId>jakarta.servlet</groupId>
			<artifactId>jakarta.servlet-api</artifactId>
			<version>${servlet-api.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>jakarta.servlet.jsp.jstl</groupId>
		    <artifactId>jakarta.servlet.jsp.jstl-api</artifactId>
		    <version>${jstl.version}</version>
		</dependency>
		<dependency>
			<groupId>org.glassfish.web</groupId>
			<artifactId>jakarta.servlet.jsp.jstl</artifactId>
			<version>3.0.1</version>
		</dependency>
		<!-- Log4J -->
		<dependency>
			<groupId>org.apache.logging.log4j</groupId>
			<artifactId>log4j-core</artifactId>
			<version>${log4j.version}</version>
		</dependency>
		<!-- Lombok -->
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<version>${lombok.version}</version>
			<scope>provided</scope>
		</dependency>
		<!-- JUnit -->
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter</artifactId>
			<version>${junit.version}</version>
			<scope>test</scope>
		</dependency>
	</dependencies>
	<build>
		<resources>
			<!-- Store build date, version and revision. -->
			<resource>
				<directory>src/main/filtered</directory>
				<filtering>true</filtering>
			</resource>
			<resource>
				<directory>src/main/resources</directory>
			</resource>
		</resources>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-enforcer-plugin</artifactId>
				<version>3.4.1</version>
				<executions>
					<execution>
						<id>enforce-versions</id>
						<goals>
							<goal>enforce</goal>
						</goals>
						<configuration>
							<rules>
								<requireJavaVersion>
									<version>17</version>
								</requireJavaVersion>
							</rules>
							<rules>
								<requireMavenVersion>
									<version>3.8</version>
								</requireMavenVersion>
							</rules>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<!-- Running JUnit tests -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>3.2.5</version>
			</plugin>

			<!-- Compute buildNumber -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>buildnumber-maven-plugin</artifactId>
				<version>3.2.0</version>
				<executions>
					<execution>
						<phase>validate</phase>
						<goals>
							<goal>create</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<doCheck>false</doCheck>
					<doUpdate>false</doUpdate>
					<format>{0}</format>
					<items>
						<item>scmVersion</item>
					</items>
					<shortRevisionLength>8</shortRevisionLength>
				</configuration>
			</plugin>

			<!-- Count lines of code. -->
			<plugin>
				<artifactId>exec-maven-plugin</artifactId>
				<groupId>org.codehaus.mojo</groupId>
				<version>3.2.0</version>
				<executions>
					<execution>
						<phase>package</phase>
						<goals>
							<goal>exec</goal>
						</goals>
						<configuration>
							<executable>bin/sloccount.sh</executable>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<!-- Report on code coverage -->
			<!-- https://mvnrepository.com/artifact/org.jacoco/jacoco-maven-plugin -->
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<executions>
					<execution>
						<id>jacoco-initialize</id>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
					</execution>
					<execution>
						<id>jacoco-report</id>
						<phase>package</phase>
						<goals>
							<goal>report</goal>
						</goals>
					</execution>
					<execution>
						<!-- Generate coverage report html in target/site/jacoco/ from target/jacoco.exec -->
						<id>report</id>
						<phase>site</phase>
						<goals>
							<goal>report</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<!-- Code style analysis -->
			<!-- http://maven.apache.org/plugins/maven-checkstyle-plugin/ -->
			<!-- mvn checkstyle:checkstyle -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-checkstyle-plugin</artifactId>
			</plugin>
			<!-- PMD and CPD reports -->
			<!-- https://maven.apache.org/plugins/maven-pmd-plugin/ -->
			<!-- mvn pmd:pmd -->
			<!-- mvn pmd:cpd -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-pmd-plugin</artifactId>
			</plugin>
			<!-- SpotBugs Static Analysis -->
			<!-- https://spotbugs.github.io/spotbugs-maven-plugin/ -->
			<!-- https://find-sec-bugs.github.io/tutorials.htm#Maven -->
			<!-- mvn spotbugs:spotbugs -->
			<plugin>
				<groupId>com.github.spotbugs</groupId>
				<artifactId>spotbugs-maven-plugin</artifactId>
				<configuration>
					<failOnError>false</failOnError>
				</configuration>
				<executions>
					<execution>
						<id>sportbugs</id>
						<goals>
							<goal>check</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-war-plugin</artifactId>
				<version>3.4.0</version>
			</plugin>

			<plugin>
				<groupId>org.codehaus.cargo</groupId>
				<artifactId>cargo-maven3-plugin</artifactId>
				<version>1.10.11</version>
				<configuration>
					<container>
						<containerId>tomcat10x</containerId>
						<type>${cargo.container.type}</type>
						<systemProperties>
							<java.util.logging.SimpleFormatter.format>%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS %4$-6s %2$s %5$s%6$s%n</java.util.logging.SimpleFormatter.format>
						</systemProperties>
					</container>
					<configuration>
						<type>${cargo.configuration.type}</type>
						<files>
							<copy>
								<file>config/tomcat10x/conf/context.xml</file>
								<tofile>conf/context.xml</tofile>
								<configfile>true</configfile>
								<overwrite>true</overwrite>
							</copy>
						</files>
						<properties>
							<cargo.server.settings>${cargo.server.settings}</cargo.server.settings>
						</properties>
					</configuration>
				</configuration>
			</plugin>
		</plugins>

		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.jacoco</groupId>
					<artifactId>jacoco-maven-plugin</artifactId>
					<version>0.8.11</version>
				</plugin>
				<plugin>
					<artifactId>maven-site-plugin</artifactId>
					<version>3.12.1</version>
					<configuration>
						<locales>fr</locales>
					</configuration>
				</plugin>
				<plugin>
					<artifactId>maven-project-info-reports-plugin</artifactId>
					<version>3.5.0</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-checkstyle-plugin</artifactId>
					<version>${checkstyle.version}</version>
					<configuration>
						<excludes>**/module-info.java</excludes>
						<includeResources>false</includeResources>
						<includeTestResources>false</includeTestResources>
						<includeTestSourceDirectory>true</includeTestSourceDirectory>
						<propertyExpansion>basedir=${basedir}</propertyExpansion>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-pmd-plugin</artifactId>
					<version>${pmd.version}</version>
					<configuration>
						<excludes>
							<exclude>target/generated-sources/*.java</exclude>
						</excludes>
					</configuration>
					<executions>
						<execution>
							<id>pmd</id>
							<goals>
								<goal>check</goal>
							</goals>
							<configuration>
								<excludeFromFailureFile>config/pmd-suppressions.properties</excludeFromFailureFile>
							</configuration>
						</execution>
						<execution>
							<id>cpd</id>
							<goals>
								<goal>cpd-check</goal>
							</goals>
							<configuration>
								<excludeFromFailureFile>config/cpd-suppressions.properties</excludeFromFailureFile>
							</configuration>
						</execution>
					</executions>
				</plugin>
				<plugin>
					<groupId>com.github.spotbugs</groupId>
					<artifactId>spotbugs-maven-plugin</artifactId>
					<version>${spotbugs.version}</version>
					<configuration>
						<xmlOutput>true</xmlOutput>
						<!-- Optional directory to put spotbugs xdoc xml report -->
						<xmlOutputDirectory>target/site</xmlOutputDirectory>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

	<reporting>
		<plugins>
			<!-- Include JaCoCo report into site -->
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<reportSets>
					<reportSet>
						<reports>
							<!-- select non-aggregate reports -->
							<report>report</report>
						</reports>
					</reportSet>
				</reportSets>
			</plugin>
			<!-- Generates API JavaDoc. -->
			<!-- mvn javadoc:javadoc -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>${javadoc.version}</version>
				<configuration>
					<links>
						<link>${javase.api.link}</link>
					</links>
					<!-- DocLint is a new feature in Java 8, which is summarized as: -->
					<!-- Provide a means to detect errors in Javadoc comments early in the
						development cycle and in a way that is easily linked back to the source code.
						This is enabled by default, and will run a whole lot of checks before generating
						Javadocs. You need to turn this off for Java 8 -->
					<additionalparam>-Xdoclint:none</additionalparam>
					<doclint>none</doclint>
					<sourceFileExcludes>
						<sourceFileExclude>module-info.*</sourceFileExclude>
					</sourceFileExcludes>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jxr-plugin</artifactId>
				<version>3.3.2</version>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-checkstyle-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-pmd-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>com.github.spotbugs</groupId>
				<artifactId>spotbugs-maven-plugin</artifactId>
			</plugin>
		</plugins>
	</reporting>
	<profiles>
		<profile>
			<id>local</id>
			<properties>
				<cargo.container.type>embedded</cargo.container.type>
				<cargo.configuration.type>standalone</cargo.configuration.type>
			</properties>
		</profile>
		<profile>
			<id>remote</id>
			<properties>
				<cargo.container.type>remote</cargo.container.type>
				<cargo.configuration.type>runtime</cargo.configuration.type>
			</properties>
		</profile>
	</profiles>
</project>
