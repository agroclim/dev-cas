package fr.inrae.agroclim.devcas;

/*-
 * #%L
 * Fake CAS
 * %%
 * Copyright (C) 2022 Agroclim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ResourceBundle;

/**
 * Application version.
 *
 * @author Olivier Maury
 */
public final class Version {
    /**
     * Resources from .properties file.
     */
    private static final ResourceBundle RESOURCES = ResourceBundle.getBundle("fr.inrae.agroclim.devcas.version");

    /**
     * @return date of JAR building
     */
    public static String getBuildDate() {
        return RESOURCES.getString("build.date");
    }

    /**
     * @return version, revision and build date
     */
    public static String getFullVersion() {
        return getVersion() + "+" + getRevision() + " " + getBuildDate();
    }

    /**
     * @return SCM revision number
     */
    public static String getRevision() {
        return RESOURCES.getString("build.number");
    }

    /**
     * Get build version.
     *
     * @return version
     */
    public static String getVersion() {
        return RESOURCES.getString("version");
    }

    /**
     * No constructor, use static methods.
     */
    private Version() {
    }

}
