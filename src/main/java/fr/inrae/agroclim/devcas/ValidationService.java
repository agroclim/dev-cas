package fr.inrae.agroclim.devcas;

/*-
 * #%L
 * Fake CAS
 * %%
 * Copyright (C) 2022 Agroclim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.UUID;

import jakarta.servlet.ServletContext;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * Ticket validation business.
 *
 * @author Olivier Maury
 */
public final class ValidationService {
    /**
     * The following values MAY be used as the “code” attribute of authentication
     * failure responses. The following is the minimum set of error codes that all
     * CAS servers MUST implement.
     *
     * @author Olivier Maury
     */
    @RequiredArgsConstructor
    public enum ErrorCode {
        /**
         * Invalid request.
         */
        INVALID_REQUEST("not all of the required request parameters were present"),
        /**
         * Invalid ticket due to specification not followed.
         */
        INVALID_TICKET_SPEC("failure to meet the requirements of validation specification"),
        /**
         * No proxy feature enabled.
         */
        UNAUTHORIZED_SERVICE_PROXY("the service is not authorized to perform proxy authentication"),
        /**
         * No proxy callback valid.
         */
        INVALID_PROXY_CALLBACK("The proxy callback specified is invalid. "
                + "The credentials specified for proxy authentication do not meet the security requirements"),
        /**
         * Invalid ticket.
         */
        INVALID_TICKET(
                "the ticket provided was not valid, or the ticket did not come from an initial login and renew was "
                        + "set on validation. The body of the <cas:authenticationFailure> block of the XML response "
                        + "SHOULD describe the exact details."),
        /**
         * Invalid service.
         */
        INVALID_SERVICE(
                "the ticket provided was valid, but the service specified did not match the service associated with "
                        + "the ticket."),
        /**
         * Server error.
         */
        INTERNAL_ERROR("an internal error occurred during ticket validation");

        /**
         * Error message.
         */
        @Getter
        private final String msg;

        /**
         * @return Validator Result for this failure.
         */
        public Result getResult() {
            return Result.err(this);
        }
    }

    /**
     * Ticket validation result.
     *
     * @author Olivier Maury
     */
    public static final class Result {
        /**
         * Builder for failure.
         *
         * @param code error code
         * @return failure Result
         */
        public static Result err(final ErrorCode code) {
            return new Result(null, code);
        }

        /**
         * Builder for success.
         *
         * @param username user name
         * @return success Result
         */
        public static Result ok(final String username) {
            return new Result(username, null);
        }

        /**
         * Username matching service and ticket.
         */
        @Getter
        private final String username;

        /**
         * Error code in case of validation failure.
         */
        @Getter
        private final ErrorCode errorCode;

        /**
         * Constructor.
         *
         * @param name username
         * @param code error code
         */
        private Result(final String name, final ErrorCode code) {
            username = name;
            errorCode = code;
        }

        /**
         * @return if validation result is failure
         */
        public boolean isErr() {
            return username == null;
        }

        /**
         * @return if validation result is success
         */
        public boolean isOk() {
            return username != null;
        }
    }

    /**
     * Delay before ticket expiration.
     */
    private static final long DEFAULT_VALIDITY_DELAY = 1000 * 60 * 5;

    /**
     * @param servletContext context for the application
     * @return the instance stored in the context
     */
    public static ValidationService getInstance(final ServletContext servletContext) {
        final String attributeName = ValidationService.class.getName();
        final Object obj = servletContext.getAttribute(attributeName);
        final ValidationService instance;
        if (obj == null) {
            instance = new ValidationService();
            servletContext.setAttribute(attributeName, instance);
        } else {
            instance = (ValidationService) obj;
        }
        return instance;
    }

    /**
     * Check if a text is null or empty or contains only spaces.
     *
     * @param text text to check
     * @return if text is blank
     */
    private static boolean isBlank(final String text) {
        return text == null || text.trim().isEmpty();
    }

    /**
     * Delay before ticket expiration.
     */
    @Setter
    private long validityDelay = DEFAULT_VALIDITY_DELAY;

    /**
     * Ticket storage.
     */
    private final Map<String, Map<String, String>> storage = new HashMap<>();

    /**
     * Create and store ticket for the service and the username.
     *
     * A service ticket is an opaque string that is used by the client as a
     * credential to obtain access to a service.
     *
     * @param service  service URL
     * @param username username
     * @return ticket
     */
    public String storeTicket(final String service, final String username) {
        final StringJoiner sj = new StringJoiner("-");
        sj.add("ST");
        sj.add(UUID.randomUUID().toString().replace("_", "").replace("-", ""));
        sj.add("" + new Date().getTime());
        final String ticket = sj.toString();
        storage.computeIfAbsent(service, key -> new HashMap<>());
        storage.get(service).put(ticket, username);
        return ticket;
    }

    /**
     * Validate the service ticket.
     *
     * @param service service URL
     * @param ticket  service ticket to validate
     * @return validation result
     */
    public Result validate(final String service, final String ticket) {
        if (isBlank(service) || isBlank(ticket)) {
            return ErrorCode.INVALID_REQUEST.getResult();
        }
        if (!ticket.startsWith("ST-")) {
            return ErrorCode.INVALID_TICKET_SPEC.getResult();
        }
        if (!storage.containsKey(service)) {
            return ErrorCode.INVALID_TICKET.getResult();
        }
        final String[] parts = ticket.split("-");
        final int size = 3;
        if (parts.length != size) {
            return ErrorCode.INVALID_TICKET_SPEC.getResult();
        }
        final long time = Long.parseLong(parts[2]);
        if (new Date().getTime() > time + validityDelay) {
            return ErrorCode.INVALID_TICKET.getResult();
        }
        final String username = storage.get(service).get(ticket);
        if (username == null) {
            // loop on all tickets, if found => wrong service is given
            for (final Map<String, String> e : storage.values()) {
                if (e.containsKey(ticket)) {
                    e.remove(ticket);
                    return ErrorCode.INVALID_SERVICE.getResult();
                }
            }
            return ErrorCode.INVALID_TICKET.getResult();
        }
        storage.get(service).remove(ticket);
        return Result.ok(username);
    }

}
