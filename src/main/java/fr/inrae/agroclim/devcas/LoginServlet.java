package fr.inrae.agroclim.devcas;

/*-
 * #%L
 * Fake CAS
 * %%
 * Copyright (C) 2022 Agroclim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;

/**
 * Credential requestor (GET) and a credential acceptor (POST).
 *
 * @author Olivier Maury
 */
@Log4j2
@WebServlet(urlPatterns = LoginServlet.PATH)
public final class LoginServlet extends HttpServlet {
    /**
     * The path for the servlet.
     */
    public static final String PATH = "/login";

    /**
     * UID.
     */
    private static final long serialVersionUID = 6900656280920343251L;

    /**
     * Add ticket parameter to service URL.
     *
     * @param service service URL
     * @param ticket  ticket value
     * @return URL of service with added ticket value
     */
    public static String buildServiceUrlWithTicket(final String service, final String ticket) {
        try {
            final URL url = new URL(service);
            final StringBuilder file = new StringBuilder();
            if (url.getPath() != null) {
                file.append(url.getPath());
            }
            file.append("?");
            if (url.getQuery() != null) {
                file.append(url.getQuery());
                file.append("&ticket=");
            } else {
                file.append("ticket=");
            }
            file.append(ticket);
            if (url.getRef() != null) {
                file.append("#");
                file.append(url.getRef());
            }
            final URL redirectUrl = new URL(url.getProtocol(), url.getHost(), url.getPort(), file.toString());
            return redirectUrl.toString();
        } catch (final MalformedURLException e) {
            return null;
        }
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException, ServletException {
        LOGGER.traceEntry();
        LOGGER.trace("Query parameters : {}", request.getParameterMap());
        String service = request.getParameter("service");
        if (service == null) {
            service = "";
        }
        // check session
        final Object sessionUsername = request.getSession().getAttribute("username");
        if (sessionUsername != null) {
            if (service.trim().isEmpty()) {
                // session OK, but no service
                request.setAttribute("username", sessionUsername);
                getServletContext().getRequestDispatcher("/WEB-INF/loggedin.jsp").forward(request, response);
                return;
            }
            // if session ok, redirect with ticket
            redirectWithTicket(response, service, (String) sessionUsername);
            return;
        }
        // if session nok, show login form
        request.setAttribute("service", service);
        getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException, ServletException {
        LOGGER.traceEntry();
        LOGGER.trace("Query parameters : {}", request.getParameterMap());
        final String service = request.getParameter("service");
        final String username = request.getParameter("username");
        request.getSession().setAttribute("username", username);
        if (service == null || service.trim().isEmpty()) {
            // session OK, but no service
            request.setAttribute("username", username);
            getServletContext().getRequestDispatcher("/WEB-INF/loggedin.jsp").forward(request, response);
            return;
        }
        redirectWithTicket(response, service, username);
    }

    /**
     * Handle request to create ticket and redirect to service.
     *
     * @param response HTTP response to redirect
     * @param service  service URL
     * @param username requested username
     * @throws IOException in case of redirect error
     */
    private void redirectWithTicket(final HttpServletResponse response, final String service, final String username)
            throws IOException {
        final ValidationService validationService = ValidationService.getInstance(getServletContext());
        // create ticket and ticket for username
        final String ticket = validationService.storeTicket(service, username);
        // redirect to URL with query parameter
        response.sendRedirect(buildServiceUrlWithTicket(service, ticket));
    }

}
