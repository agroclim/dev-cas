package fr.inrae.agroclim.devcas;

/*-
 * #%L
 * Fake CAS
 * %%
 * Copyright (C) 2022 Agroclim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import fr.inrae.agroclim.devcas.ValidationService.Result;
import lombok.extern.log4j.Log4j2;

/**
 * Check the validity of a service ticket.
 *
 * Part of CAS 1.0.
 *
 * @author Olivier Maury
 */
@Log4j2
@WebServlet(urlPatterns = ValidateCAS1Servlet.PATH)
public final class ValidateCAS1Servlet extends HttpServlet {
    /**
     * The path for the servlet.
     */
    public static final String PATH = "/validate";

    /**
     * UID.
     */
    private static final long serialVersionUID = 6900656280920343253L;

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException, ServletException {
        LOGGER.traceEntry();
        final String service = request.getParameter("service");
        final String ticket = request.getParameter("ticket");
        final ValidationService validationService = ValidationService.getInstance(getServletContext());
        final Result result = validationService.validate(service, ticket);
        if (result.isOk()) {
            response.getWriter().write("yes\n");
            response.getWriter().write(result.getUsername());
            response.getWriter().write("\n");
        } else {
            LOGGER.trace(result.getErrorCode());
            response.getWriter().write("no\n");
        }
    }
}
