package fr.inrae.agroclim.devcas;

/*-
 * #%L
 * Fake CAS
 * %%
 * Copyright (C) 2022 Agroclim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import fr.inrae.agroclim.devcas.ValidationService.Result;
import lombok.extern.log4j.Log4j2;

/**
 * Check the validity of a service ticket and returns an XML-fragment response.
 *
 * Part of CAS 2.0.
 *
 * @author Olivier Maury
 */
@Log4j2
@WebServlet(urlPatterns = ValidateCAS2Servlet.PATH)
public final class ValidateCAS2Servlet extends HttpServlet {
    /**
     * The path for the servlet.
     */
    public static final String PATH = "/serviceValidate";

    /**
     * The XML response in case of validation failure.
     */
    private static final String FAILURE_XML = "<cas:serviceResponse xmlns:cas=\"http://www.yale.edu/tp/cas\">%n"
            + " <cas:authenticationFailure code=\"%s\"><![CDATA[%s]]></cas:authenticationFailure>%n" //
            + "</cas:serviceResponse>";

    /**
     * The XML response in case of validation success.
     */
    private static final String SUCCESS_XML = "<cas:serviceResponse xmlns:cas=\"http://www.yale.edu/tp/cas\">%n"
            + " <cas:authenticationSuccess>%n" //
            + "  <cas:user>%s</cas:user>%n" //
            + "  <cas:proxyGrantingTicket></cas:proxyGrantingTicket>%n" //
            + " </cas:authenticationSuccess>%n" //
            + "</cas:serviceResponse>";

    /**
     * UID.
     */
    private static final long serialVersionUID = 6900656280920343254L;

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException, ServletException {
        LOGGER.traceEntry();
        final String service = request.getParameter("service");
        final String ticket = request.getParameter("ticket");
        final ValidationService validationService = ValidationService.getInstance(getServletContext());
        final Result result = validationService.validate(service, ticket);
        response.setContentType("text/xml; charset=utf-8");
        if (result.isOk()) {
            response.getWriter().write(String.format(SUCCESS_XML, result.getUsername()));
        } else {
            LOGGER.trace(result.getErrorCode());
            response.getWriter()
            .write(String.format(FAILURE_XML, result.getErrorCode().name(), result.getErrorCode().getMsg()));
        }
    }
}
