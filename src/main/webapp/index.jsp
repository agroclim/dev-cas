<%--
  #%L
  Fake CAS
  %%
  Copyright (C) 2022 Agroclim
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${param.lang}" />
<fmt:setBundle basename="messages" />
<fmt:requestEncoding value = "UTF-8" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:page>
	<jsp:attribute name="description"><fmt:message key="index.description" /></jsp:attribute>
	<jsp:attribute name="h1"><fmt:message key="index.h1" /></jsp:attribute>
	<jsp:attribute name="title"><fmt:message key="index.title" /></jsp:attribute>
	<jsp:body>
		<dl>
			<dt>
				<a href="login">/login</a>
			</dt>
			<dd><fmt:message key="index.description.login" /></dd>
			<dt>
				<a href="logout">/logout</a>
			</dt>
			<dd><fmt:message key="index.description.logout" /></dd>
			<dt>
				<a href="validate">/validate</a> [CAS 1.0]
			</dt>
			<dd><fmt:message key="index.description.validate" /></dd>
			<dt>
				<a href="serviceValidate">/serviceValidate</a> [CAS 2.0]
			</dt>
			<dd><fmt:message key="index.description.serviceValidate" /></dd>
		</dl>
	</jsp:body>
</t:page>
