<%--
  #%L
  Fake CAS
  %%
  Copyright (C) 2022 Agroclim
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@ attribute name="description" fragment="true"%>
<%@ attribute name="h1" fragment="true"%>
<%@ attribute name="title" fragment="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${param.lang}" />
<fmt:setBundle basename="messages" />
<fmt:requestEncoding value="UTF-8" />
<%
request.setAttribute("version", fr.inrae.agroclim.devcas.Version.getFullVersion());
%>
<!doctype html>
<html lang="${param.lang}">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta name="description" content="<jsp:invoke fragment="description" />" />
<title><jsp:invoke fragment="title" /></title>
<link rel="stylesheet" media="screen" href="style.css">
</head>
<body>
	<div class="container">
		<h1>
			<jsp:invoke fragment="h1" />
		</h1>
		<jsp:doBody />
	</div>
	<footer>
		${version}
		<c:choose>
			<c:when test='${param.lang == "en"}'>
				<a href="?lang=fr">Cette page en français.</a>
			</c:when>
			<c:otherwise>
				<a href="?lang=en">This page in English.</a>
			</c:otherwise>
		</c:choose>
	</footer>
</body>
</html>
