<%--
  #%L
  Fake CAS
  %%
  Copyright (C) 2022 Agroclim
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${param.lang}" />
<fmt:setBundle basename="messages" />
<fmt:requestEncoding value="UTF-8" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:page>
	<jsp:attribute name="description">
		<fmt:message key="login.description" />
	</jsp:attribute>
	<jsp:attribute name="h1">
		<fmt:message key="login.h1" />
	</jsp:attribute>
	<jsp:attribute name="title">
		<fmt:message key="login.title" />
	</jsp:attribute>
	<jsp:body>
	<form method="post" action="login">
		<c:choose>
			<c:when test='${service != ""}'>
				<p>
					<fmt:message key="login.continue">
						<fmt:param value="${service}" />
					</fmt:message>
				</p>
			</c:when>
		</c:choose>
		<label for="username"><fmt:message key="login.label.username" /></label>
		<!--  -->
		<input type="text" id="username" name="username"
				placeholder="<fmt:message key="login.placeholder.username" />"
				required />
		<!--  -->
		<input type="hidden" name="service" value="${service}" />
		<!--  -->
		<input type="submit" id="submit" onclick="storeUsername()" />
		<!--  -->
		<ul id="usernames">
		</ul>
	</form>
	<script>
function localStorageAvailable() {
	return window['localStorage'] != undefined;
}
function removeUsername(username) {
	if (!localStorageAvailable()) {
		return;
	}
	var list = JSON.parse(window.localStorage.getItem("usernames"));
	if (list != null) {
		var filtered = list.filter(function(value, index, arr){ 
			return value != username;
		});
		window.localStorage.setItem("usernames", JSON.stringify(filtered));
	}
}
function storeUsername() {
	if (!localStorageAvailable()) {
		return;
	}
	var input = document.getElementById("username");
	if (input == null) {
		console.error("Strange, no element with id=username");
		return;
	}
	var username = input.value;
	var list = JSON.parse(window.localStorage.getItem("usernames"));
	if (list == null) {
		list = [username];
	} else {
		list.push(username);
	}
	window.localStorage.setItem("usernames", JSON.stringify([...new Set(list)].sort()));
}
if (localStorageAvailable()) {
	var list = JSON.parse(window.localStorage.getItem("usernames"));
	console.log(list);
	var elem = document.getElementById("usernames");
	if (list != null) {
		if (elem != null) {
			var info = document.createElement('li');
			info.innerText = "<fmt:message key="login.label.previouslogins" />";
			elem.append(info);
			for (const username of list) {
				console.log(username);
				const li = document.createElement('li');
				var text = document.createElement('span');
				text.innerText = username;
				text.className = 'username';
				text.onclick = function() {
					var btn = document.getElementById("submit");
					if (btn == null) {
						console.error("Strange, no element with id=submit");
						return;
					}
					var input = document.getElementById("username");
					if (input == null) {
						console.error("Strange, no element with id=username");
						return;
					}
					input.value = this.innerText;
					btn.click();
					btn.disabled = true;
				};
				li.append(text);
				var times = document.createElement('span');
				times.innerHTML = 'X';
				times.className = 'remove';
				times.onclick = function() {
					elem.removeChild(li);
					removeUsername(username);
				};
				li.append(times);
				elem.append(li);
			}
		} else {
			console.error('Strange, no element with id=usernames');
		}
	}
}
	</script>
	</jsp:body>
</t:page>
