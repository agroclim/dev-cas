package fr.inrae.agroclim.devcas.tests;

/*-
 * #%L
 * Fake CAS
 * %%
 * Copyright (C) 2022 Agroclim
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.inrae.agroclim.devcas.ValidationService;
import fr.inrae.agroclim.devcas.ValidationService.ErrorCode;
import fr.inrae.agroclim.devcas.ValidationService.Result;

/**
 * Test for {@link ValidationService}.
 *
 * @author Olivier Maury
 */
public class ValidationServiceTest {
    /**
     * Username to test.
     */
    private static final String USERNAME = "jdoe";

    /**
     * Service URL to test.
     */
    private static final String SERVICE_URL = "http://localhost:8080/test";

    /**
     * Service instance to test.
     */
    private ValidationService service;

    /**
     * Reset service.
     */
    @BeforeEach
    public void before() {
        service = new ValidationService();
    }

    /**
     * In case of invalid service, CAS MUST invalidate the ticket and disallow
     * future validation of that same ticket.
     */
    @Test
    void invalidService() {
        final String newServiceUrl = SERVICE_URL + "new";
        service.storeTicket(newServiceUrl, USERNAME);
        final String ticket = service.storeTicket(SERVICE_URL, USERNAME);
        assertNotNull(ticket);
        final Result result = service.validate(newServiceUrl, ticket);
        assertTrue(result.isErr());
        assertEquals(ErrorCode.INVALID_SERVICE, result.getErrorCode());
    }

    /**
     * Services MUST be able to accept service tickets of up to 32 characters in
     * length. It is RECOMMENDED that services support service tickets of up to 256
     * characters in length.
     */
    @Test
    void ticketSize() {
        final int minSize = 32;
        final String ticket = service.storeTicket(SERVICE_URL, USERNAME);
        assertTrue(ticket.length() > minSize);
    }

    /**
     * Service tickets MUST begin with the characters, ST-.
     */
    @Test
    void ticketStart() {
        final String ticket = service.storeTicket(SERVICE_URL, USERNAME);
        assertNotNull(ticket);
        assertTrue(ticket.startsWith("ST-"));
    }

    /**
     * CAS SHOULD expire unvalidated service tickets in a reasonable period of time
     * after they are issued. If a service presents an expired service ticket for
     * validation, CAS MUST respond with a validation failure response.
     */
    @Test
    void validityDelay() {
        final String ticket = service.storeTicket(SERVICE_URL, USERNAME);
        Result result = service.validate(SERVICE_URL, ticket);
        assertTrue(result.isOk());
        assertFalse(result.isErr());
        service.setValidityDelay(0L);
        result = service.validate(SERVICE_URL, ticket);
        assertFalse(result.isOk());
    }

    /**
     * Service tickets MUST only be valid for one ticket validation attempt. Whether
     * or not validation was successful, CAS MUST then invalidate the ticket,
     * causing all future validation attempts of that same ticket to fail.
     */
    @Test
    void validOnce() {
        final String ticket = service.storeTicket(SERVICE_URL, USERNAME);
        assertNotNull(ticket);
        Result result = service.validate(SERVICE_URL, ticket);
        assertTrue(result.isOk(), "" + result.getErrorCode());
        assertEquals(USERNAME, result.getUsername());
        result = service.validate(SERVICE_URL, ticket);
        assertFalse(result.isOk());
        assertNotNull(result.getErrorCode().getMsg());
    }
}
