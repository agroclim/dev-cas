package fr.inrae.agroclim.devcas.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import fr.inrae.agroclim.devcas.LoginServlet;

/**
 * Test for {@link LoginServlet}.
 *
 * @author Olivier Maury
 */
class LoginServletTest {

    @Test
    void buildServiceUrlWithTicket() {
        final String ticket = "abcd";
        final Map<String, String> tests = new HashMap<>();
        tests.put("http://localhost/", //
                "http://localhost/?ticket=" + ticket);
        tests.put("http://localhost:8080/siclima/", //
                "http://localhost:8080/siclima/?ticket=" + ticket);
        tests.put("http://localhost:8080/siclima/?consent=true", //
                "http://localhost:8080/siclima/?consent=true&ticket=" + ticket);
        tests.put("http://localhost:8080/siclima/?consent=true#anchor",
                "http://localhost:8080/siclima/?consent=true&ticket=" + ticket + "#anchor");
        tests.forEach((service, expected) -> {
            final String actual = LoginServlet.buildServiceUrlWithTicket(service, ticket);
            assertEquals(expected, actual);
        });
    }
}
