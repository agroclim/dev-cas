#!/bin/bash
BIN_DIR=$(dirname $0)
ROOT_DIR=$(dirname $BIN_DIR)
TOKEI2CLOC=$BIN_DIR/tokei2cloc.py
TOKEI=$(command -v tokei)
TOKEI_VERSION="12.1.2"

function check_tokei_version() {
	echo "Checking tokei version"
	VERSION=$($TOKEI --version | awk '{print $2}')
	echo "tokei version: $VERSION"
	echo "tokei expected version: $TOKEI_VERSION"
	if [ "$TOKEI_VERSION" == "$VERSION" ]; then
		return 0
	fi
	return 1
}

function install_tokei() {
	echo "Installing tokei"
	TOKEITGZ=tokei-x86_64-unknown-linux-gnu.tar.gz
	TOKEITMP=~/tmp/tokei-$TOKEI_VERSION.tar.gz
	if [ -f $TOKEITMP ]; then
		if ! gzip -t $TOKEITMP; then
			echo "$TOKEITMP is not a valid gzip archive"
			rm -f $TOKEITMP
		fi
	fi
	if [ ! -f $TOKEITMP ]; then
		mkdir -p ~/tmp
		URL="https://github.com/XAMPPRocky/tokei/releases/download/v$TOKEI_VERSION/$TOKEITGZ"
		echo "Downloading tokei from $URL"
		curl --location $URL -o $TOKEITMP
	fi
	echo "Extracting tokei"
	tar zxf $TOKEITMP -C $BIN_DIR
	TOKEI=$BIN_DIR/tokei
}

PYTHON=$(command -v python)
if [ ! -x "$PYTHON" ]; then
	PYTHON=$(command -v python3)
fi
if [ ! -x "$PYTHON" ]; then
	echo "Strange, python not found!"
	exit 1
fi

if [ ! -x "$TOKEI" ]; then
	if [ -f $BIN_DIR/tokei ]; then
		TOKEI=$BIN_DIR/tokei
	fi
fi

if [ -x "$TOKEI" ]; then
	check_tokei_version
	if [ $? -eq 1 ]; then
		install_tokei
	fi
fi
if [ ! -x "$TOKEI" ]; then
	install_tokei
fi
if [ ! -f $TOKEI ]; then
	echo "Strange, $TOKEI does not exist!"
	exit 1
fi

echo "tokei is installed at $TOKEI"
$TOKEI --version
mkdir -p $ROOT_DIR/target
$TOKEI -f -o json $ROOT_DIR/src | $PYTHON $TOKEI2CLOC > $ROOT_DIR/target/cloc.xml
