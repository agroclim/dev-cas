#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

# $Id$
#
# Author : Olivier Maury
# Creation Date : 2019-01-15 10:30:29 +0200
# Last Revision : $Date$
# Revision : $Rev$
u"""
[1mNOM[m
        %prog - Tokei2Cloc

[1mDESCRIPTION[m
        Formate la sortie de tokei dans le format de Cloc

[1mEXEMPLE[m
        tokei -f -o json bin src | %prog

[1mAUTEUR[m
        Olivier Maury

[1mVERSION[m
        $Date$
"""

__revision__ = "$Rev$"
__author__ = "Olivier Maury"
import json
import sys

results = json.loads(sys.stdin.read())

# header
nb_files = 0
nb_lines = 0
for lang in results:
    nb_files += len(results[lang]['reports'])
    nb_lines += int(results[lang]['blanks'])
    nb_lines += int(results[lang]['code'])
    nb_lines += int(results[lang]['comments'])

print("""<?xml version="1.0"?><results>
<header>
  <cloc_url>http://cloc.sourceforge.net</cloc_url>
  <cloc_version>1.60</cloc_version>
  <elapsed_seconds>0.348630905151367</elapsed_seconds>
  <n_files>%d</n_files>
  <n_lines>%d</n_lines>
  <files_per_second>510.568619619987</files_per_second>
  <lines_per_second>88902.0438005723</lines_per_second>
  <report_file>target/cloc.xml</report_file>
</header>
<files> """ % (nb_files, nb_lines))

# files
total_blank = 0
total_comment = 0
total_code = 0
for lang in results:
    for result in results[lang]['reports']:
        blank = int(result['stats']['blanks'])
        comment = int(result['stats']['comments'])
        code = int(result['stats']['code'])
        print("""  <file name="%s" blank="%d" comment="%d" code="%d"  language="%s" />""" % 
            (result['name'], blank, comment, code, lang))
        total_blank += blank
        total_comment += comment
        total_code += code

# total
print("""  <total blank="%d" comment="%d" code="%d" />
</files>
</results>""" % (total_blank, total_comment, total_code))
